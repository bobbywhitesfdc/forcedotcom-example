# forcedotcom_sonar
Shell project for Force.com Apex Development with Sonar Code-Scan integration

How to get started:

1) Clone the repository
2) Edit build.properties to set your Salesforce userid/password/url
3) Edit sonar.properties to give your project a name that you'll see in Sonar
4) Run the build with ANT ( no target specified will yank the Code)
5) Run the Sonar target:  ant sonar

